//
//  HomeViewController.m
//  XYCQ
//
//  Created by 袁杰 on 2017/8/9.
//  Copyright © 2017年 BiggerMax. All rights reserved.
//

#import "HomeViewController.h"
#import "SDCycleScrollView.h"
#import "NewsChildController.h"
#import "HomeCell1.h"
#import "RecycleCell.h"



@interface HomeViewController ()<UIScrollViewDelegate,SDCycleScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
	NSInteger _selIndex;
	NSInteger _lastIndex;
}
@property(nonatomic,strong)SDCycleScrollView *cycleView;
@property(nonatomic,strong)UIScrollView *scroll_nav;
@property(nonatomic,strong)UIScrollView *mainScrollView;
@property(nonatomic,copy)NSArray *navArray;

@property (strong, nonatomic) NSMutableArray *cateBtnArray;//分类按钮数组
@end

@implementation HomeViewController
#define  btnWidth 72

-(SDCycleScrollView *)cycleView
{
	if (!_cycleView) {
		SDCycleScrollView *cycleView = [[SDCycleScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 220)];
		cycleView.backgroundColor = [UIColor clearColor];
		NSArray *imageArray=[NSArray arrayWithObjects:@"banner",@"banner",@"banner", nil];
		cycleView.localizationImageNamesGroup = imageArray;
		cycleView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
		cycleView.currentPageDotColor = [UIColor parse:@"#00C6C9"];
		cycleView.pageDotColor=[UIColor whiteColor];
		cycleView.placeholderImage = [UIImage imageNamed:@"default_image"];
		if (imageArray.count>1) {
			cycleView.autoScroll=TRUE;
			cycleView.autoScrollTimeInterval=2;
		}
		self.cycleView = cycleView;
		[self.view addSubview:cycleView];
	}
	return _cycleView;
	
}
-(UIScrollView *)scroll_nav
{
	if (!_scroll_nav) {
		UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(9, 232, ScreenWidth-18, 40)];
		scrollView.contentSize =CGSizeMake(self.navArray.count * btnWidth, 0);
		scrollView.showsHorizontalScrollIndicator = false;
		scrollView.showsVerticalScrollIndicator = false;
		scrollView.backgroundColor = [UIColor whiteColor];
		scrollView.pagingEnabled = NO;
			//scrollView.delegate = self;
	
		//蓝线
		UIView *blueLine = [[UIView alloc] initWithFrame:CGRectMake(0, 38, self.navArray.count * btnWidth, 2)];
		blueLine.backgroundColor = [UIColor parse:@"#00C6C9"];
		[scrollView addSubview:blueLine];
		CGRect lastBtnRect = CGRectZero;//上个btn宽
		for (int i=0; i<self.navArray.count; i++)
		{
			UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
			btn.frame = CGRectMake(lastBtnRect.origin.x+lastBtnRect.size.width, 0, btnWidth, 40);
			[btn setTitle:self.navArray[i] forState:UIControlStateNormal];
			[btn setTitleColor:[UIColor parse:@"#00C6C9"] forState:UIControlStateNormal];
			[btn setTitle:self.navArray[i] forState:UIControlStateSelected];
			UIImage *bgImage = [UIImage imageWithColor:[UIColor parse:@"#00C6C9"] size:btn.bounds.size] ;
			[btn setBackgroundImage:bgImage forState:UIControlStateSelected];
			[btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
			[btn.titleLabel setFont:Font(18)];
			[btn setTag:i];
			if (btn.tag==_selIndex) {
				btn.selected = YES;
			}
			[btn addTarget:self action:@selector(cateBtnAction:) forControlEvents:UIControlEventTouchUpInside];
			[scrollView addSubview:btn];
			lastBtnRect = btn.frame;
			[self.cateBtnArray addObject:btn];
		}
		[self.view addSubview:scrollView];
		self.scroll_nav = scrollView;
	}
	return _scroll_nav;
}
-(UIScrollView *)mainScrollView
{
	if (!_mainScrollView) {
		UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(9, 272, ScreenWidth-18, ScreenHeight-64-272)];
		scrollView.contentSize = CGSizeMake(self.navArray.count * ScreenWidth, 0);
		scrollView.backgroundColor = [UIColor whiteColor];
		scrollView.showsVerticalScrollIndicator = false;
		scrollView.showsHorizontalScrollIndicator = false;
		scrollView.delegate = self;
		scrollView.pagingEnabled = YES;
		scrollView.bounces = NO;
		scrollView.delaysContentTouches = NO;
		[self scrollViewDidEndScrollingAnimation:scrollView];
		[self.view insertSubview:scrollView atIndex:0];
		UITableView *tableView = [[UITableView alloc] initWithFrame:scrollView.bounds style:UITableViewStyleGrouped];
			tableView.delegate = self;
			tableView.dataSource = self;
			[tableView registerClass:[HomeCell1 class] forCellReuseIdentifier:@"HomeCell1"];
			tableView.backgroundColor = [UIColor clearColor];
		tableView.showsVerticalScrollIndicator = false;
		tableView.sectionHeaderHeight = 0;
		tableView.separatorStyle = NO;
		tableView.rowHeight = 70;
		tableView.tag = 0;
		[scrollView addSubview:tableView];
		for (int i = 0; i < _navArray.count-1; i ++ ) {
			NewsChildController *childVC = [NewsChildController new];
			[self addChildViewController:childVC];
		}
		
		self.mainScrollView = scrollView;
	}
	return _mainScrollView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
	_selIndex = 0 ;
	self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
	self.cateBtnArray = [NSMutableArray array];
	self.navArray = [NSArray arrayWithObjects:@"精选",@"新闻",@"活动",@"公告",@"山海经",@"礼品" ,nil];
	if ([self respondsToSelector:@selector( setAutomaticallyAdjustsScrollViewInsets:)]) {
		self.automaticallyAdjustsScrollViewInsets = NO;
	}
	[self setupUI];
	
}
-(void)setupUI
{
	[self cycleView];
	[self scroll_nav];
	[self mainScrollView];
}
-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	self.navigationItem.title = @"轩辕传奇";
}

-(void)cateBtnAction:(UIButton *)button
{
	NSInteger index = button.tag;
	button.selected = !button.selected;
	UIButton *lastBtn = self.cateBtnArray[_lastIndex];
	lastBtn.selected = !lastBtn.selected;
	[UIView animateWithDuration:0.05 animations:^{
		CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
		animation.keyPath = @"transform.scale";
		animation.values = @[@1.0,@1.3,@0.9,@1.02,@1.0];
		animation.duration = 1;
		animation.calculationMode = kCAAnimationCubic;
		[button.layer addAnimation:animation forKey:nil];
	}];
	CGPoint offset = self.mainScrollView.contentOffset;
	offset.x = button.tag * self.mainScrollView.width;
	[self.mainScrollView setContentOffset:offset animated:YES];
	_lastIndex = index;
//	if (index != 0) {
//		NewsChildController *newsVC = [self.childViewControllers objectAtIndex:index-1];
//		newsVC.view.tag = index;
//		newsVC.view.frame = self.mainScrollView.bounds;
//		[self.mainScrollView addSubview:newsVC.view];
//	}
}

#pragma mark --UITableView
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 50;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *header = [[UIView alloc] initWithFrame:CGRectMake(10, 5, ScreenWidth-20, 40)];
	header.backgroundColor = [UIColor clearColor];
	YYLabel *label = [[YYLabel alloc] initWithFrame:CGRectMake(10, 10, header.width-20, header.height-5)];
	label.backgroundColor = [UIColor parse:@"#E8F5F1"];
	label.text = @" 情缘、结义一网打尽《轩辕传奇手游》8月...";
	label.font = [UIFont systemFontOfSize:20 weight:5];
	[header addSubview:label];
	return header;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 5;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	HomeCell1 *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeCell1"];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	if (!cell) {
		cell = [[HomeCell1 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HomeCell1"];
	}
	return cell;
}
#pragma mark -- ScrollViewDelegate
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
	NSInteger index = scrollView.contentOffset.x / scrollView.width;
	NSInteger count = round(ScreenWidth/btnWidth);

	if (index >= count) {
		_scroll_nav.contentOffset = CGPointMake((index-2)*btnWidth, 0);
	}else
	{
		_scroll_nav.contentOffset = CGPointMake(0, 0);
	}

	if (index == 0){}
	else{
		NewsChildController *newsVC = [self.childViewControllers objectAtIndex:index-1];
		newsVC.view.tag = index;
		newsVC.view.left = scrollView.contentOffset.x;
		newsVC.view.top = 0;
		newsVC.view.height = scrollView.height;
		newsVC.view.width = scrollView.width;
		[scrollView addSubview:newsVC.view];
	}

	
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	[self scrollViewDidEndScrollingAnimation:scrollView];
	if (_mainScrollView ==scrollView) {
		NSInteger index = scrollView.contentOffset.x / scrollView.width;
		UIButton *btn = self.cateBtnArray[index];
		[self cateBtnAction:btn];
	}
	
	
}
@end

