//
//  NewsChildController.m
//  XYCQ
//
//  Created by 袁杰 on 2017/8/10.
//  Copyright © 2017年 BiggerMax. All rights reserved.
//

#import "NewsChildController.h"
#import "HomeCell1.h"
@interface NewsChildController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *tableView;
@end

@implementation NewsChildController

- (void)viewDidLoad {
    [super viewDidLoad];
	_tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
	_tableView.backgroundColor = RandomColor;
	self.tableView.delegate = self;
	self.tableView.dataSource = self;
	self.tableView.rowHeight = 70;
	self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	[self.tableView registerClass:[HomeCell1 class] forCellReuseIdentifier:@"HomeCell1"];
	[self.view addSubview:self.tableView];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 8;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	HomeCell1 *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeCell1"];
	if (!cell) {
		cell = [[HomeCell1 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HomeCell1"];
	}
	return cell;
}

@end
