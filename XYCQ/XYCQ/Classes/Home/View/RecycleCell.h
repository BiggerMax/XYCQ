//
//  RecycleCell.h
//  Lottery
//
//  Created by 袁杰 on 2017/7/24.
//  Copyright © 2017年 BiggerMax. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"
@interface RecycleCell : UITableViewCell
@property (weak, nonatomic) IBOutlet SDCycleScrollView *noticeView;

-(void) updateNoticeView:(NSArray *)imagesURLStrings;
@end
