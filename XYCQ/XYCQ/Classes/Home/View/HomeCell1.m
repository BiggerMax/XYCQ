//
//  HomeCell1.m
//  XYCQ
//
//  Created by 袁杰 on 2017/8/10.
//  Copyright © 2017年 BiggerMax. All rights reserved.
//

#import "HomeCell1.h"

@interface HomeCell1 ()
@property(nonatomic,strong)UILabel *title;
@property(nonatomic,strong)UILabel *category;
@property(nonatomic,strong)UILabel *date;
@end
@implementation HomeCell1

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
		_category = [UILabel new];
		UIColor *color = [UIColor parse:@"#00C6C9"];
		[_category setBackgroundColor:color];
		_category.textAlignment = NSTextAlignmentCenter;
		_category.textColor = [UIColor whiteColor];
		_category.layer.cornerRadius = 12;
		_category.layer.masksToBounds = YES;
		_category.text = @"新闻";
		[self addSubview:_category];
		
		_title = [UILabel new];
		_title.font = [UIFont systemFontOfSize:18];
		_title.textColor = [UIColor blackColor];
		_title.textAlignment = NSTextAlignmentLeft;
		_title.text = @"这只神兽有点吊--烛龙";
		[self addSubview:_title];
		
		_date = [UILabel new];
		_date.textColor = [UIColor darkTextColor];
		_date.textAlignment = NSTextAlignmentLeft;
		_date.font = [UIFont systemFontOfSize:13];
		_date.text = @"08/09";
		[self addSubview:_date];
		
		UIView *sepLine = [[UIView alloc] initWithFrame:CGRectMake(10, 68, ScreenWidth-40, 1)];
		sepLine.backgroundColor = [UIColor groupTableViewBackgroundColor];
		[self addSubview:sepLine];
		
		[_category mas_makeConstraints:^(MASConstraintMaker *make) {
			make.leading.mas_equalTo(self).offset(10);
			make.top.mas_equalTo(self).offset(10);
			make.height.equalTo(@25);
			make.width.equalTo(@50);
		}];
		[_title mas_makeConstraints:^(MASConstraintMaker *make) {
			make.leading.mas_equalTo(_category.mas_trailing).offset(10);
			make.centerY.mas_equalTo(_category.mas_centerY);
			make.trailing.mas_equalTo(self).offset(-10);
			make.height.equalTo(@30);
		}];
		[_date mas_makeConstraints:^(MASConstraintMaker *make) {
			make.leading.mas_equalTo(_title);
			make.top.mas_equalTo(_title.mas_bottom);
			make.width.equalTo(@100);
			make.height.equalTo(@15);
		}];
	}
	return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
