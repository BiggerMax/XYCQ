//
//  RecycleCell.m
//  Lottery
//
//  Created by 袁杰 on 2017/7/24.
//  Copyright © 2017年 BiggerMax. All rights reserved.
//

#import "RecycleCell.h"

@implementation RecycleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void) updateNoticeView:(NSArray *)imagesURLStrings
{
    // 商品广告页面不需要文字
    self.noticeView.localizationImageNamesGroup = imagesURLStrings;
    //self.noticeView.imageURLStringsGroup = imagesURLStrings;
    self.noticeView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    self.noticeView.currentPageDotColor = [UIColor whiteColor]; // 自定义分页控件小圆标颜色
    self.noticeView.pageDotColor=[UIColor colorWithWhite:1 alpha:0.4];
    self.noticeView.placeholderImage = [UIImage imageNamed:@"default_image"];
    if (imagesURLStrings.count>1) {
        self.noticeView.autoScroll=TRUE;
        self.noticeView.autoScrollTimeInterval=2;
    }
}

@end
