//
//  StrategyViewController.m
//  XYCQ
//
//  Created by 袁杰 on 2017/8/10.
//  Copyright © 2017年 BiggerMax. All rights reserved.
//

#import "StrategyViewController.h"
#import "ZHNewViewController.h"
#import "ZHNoviceViewController.h"
#import "ZHProfessionalViewController.h"
#import "ZHPlayViewController.h"
#import "ZHGrandViewController.h"
@interface StrategyViewController ()<VTMagicViewDelegate,VTMagicViewDataSource>
@property (nonatomic,strong)VTMagicController *magicController;
@property (nonatomic,strong)NSArray *menuList;
@end

@implementation StrategyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"攻略";
    self.view.backgroundColor = [UIColor whiteColor];
    //数据
    [self initDataSource];
    //界面设置
    [self initUserInterface];
}
//界面设置
- (void)initUserInterface{
    
    
    
    
    //背景颜色
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeAll;
    [self addChildViewController:self.magicController];
    [self.view addSubview:_magicController.view];
    [self.view setNeedsUpdateConstraints];
    [_magicController.magicView reloadData];


}
//数据
- (void)initDataSource{
    
    _menuList = @[@"最新",@"新手",@"职业",@"玩法",@"战报"];
}

//点击跳转视图控制器
- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex{
    
    switch (pageIndex) {
        case 0:{
            //最新
            ZHNewViewController *vc = [[ZHNewViewController alloc] init];
            return vc;
        }
        case 1:{
            //新手
            ZHNoviceViewController *vc = [[ZHNoviceViewController alloc] init];
            return vc;
        }
        case 2:{
            //职业
            ZHProfessionalViewController *vc = [[ZHProfessionalViewController alloc] init];
            return vc;
            
        }
        case 3:{
            //玩法
            ZHPlayViewController  *vc = [[ZHPlayViewController alloc] init];
            return vc;
        }
        case 4:{
            //战报
            ZHGrandViewController *vc = [[ZHGrandViewController alloc] init];
            return vc;
        }
            break;
        default:
            break;
    }
    return nil;
}
#pragma mark - delegate
//菜单标题
- (NSArray<NSString *> *)menuTitlesForMagicView:(VTMagicView *)magicView {
    return _menuList;
}
//点击时button的变化
- (UIButton *)magicView:(VTMagicView *)magicView menuItemAtIndex:(NSUInteger)itemIndex{
    
    static NSString *itemIdentifer = @"itemIdentifer";
    UIButton *menuItem = [magicView dequeueReusableItemWithIdentifier:itemIdentifer];
    if (!menuItem) {
        menuItem = [UIButton buttonWithType:UIButtonTypeCustom];
        [menuItem setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [menuItem setTitleColor:[UIColor parse:@"00C6C9"] forState:UIControlStateSelected];
        menuItem.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:10.f];
    }
    return menuItem;
}
#pragma mark - accessor methods

- (VTMagicController *)magicController {
    if (!_magicController) {
        _magicController = [[VTMagicController alloc] init];
        _magicController.view.translatesAutoresizingMaskIntoConstraints = YES;
        //导航栏的颜色
        _magicController.magicView.navigationColor = [UIColor whiteColor];
        //滑动条的颜色
        _magicController.magicView.sliderColor = [UIColor parse:@"00C6C9"];
        _magicController.magicView.switchStyle = VTSwitchStyleDefault;
        _magicController.magicView.layoutStyle = VTLayoutStyleDivide;
        //_magicController.magicView.navigationHeight = AAdaption(44);
        //        _magicController.magicView.headerView.frame = AAdaptionRectFromFrame(CGRectMake(0, 0, 375, 24));
        //        _magicController.magicView.headerView.hidden = NO;
        //        _magicController.magicView.headerView.backgroundColor = [UIColor whiteColor];
        // _magicController.magicView.leftNavigatoinItem = self.back;
        _magicController.magicView.headerHidden = YES;
        _magicController.magicView.againstStatusBar = NO;
        _magicController.magicView.sliderExtension = 20.0;
        _magicController.magicView.delegate = self;
        _magicController.magicView.dataSource = self;
        _magicController.magicView.itemScale = 1.3;//点中的item变大
        
    }
    return _magicController;
}


@end
