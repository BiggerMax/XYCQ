//
//  ZHProfessionalViewController.m
//  XYCQ
//
//  Created by zhangliwen on 2017/8/10.
//  Copyright © 2017年 BiggerMax. All rights reserved.
//

#import "ZHProfessionalViewController.h"
#import "ZHNewTableViewCell.h"
@interface ZHProfessionalViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)RefreshLoadMoreTableView *tableView;
//dataSource
@property (nonatomic,strong)NSMutableArray *dataSource;


@end

@implementation ZHProfessionalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"职业";
    _dataSource = [NSMutableArray array];
    self.view.backgroundColor = [UIColor whiteColor];
    //加载tableView
    [self.view addSubview:self.tableView];
    //网络请求
    [self initData];
}
//网络请求
- (void)initData{
    
    
}
#pragma tableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ZHNewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"identifer"];
    //关闭cell的点击
    cell.selectionStyle = UITableViewCellEditingStyleNone;
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (RefreshLoadMoreTableView *)tableView{
    
    if (!_tableView) {
        _tableView = [[RefreshLoadMoreTableView alloc] initWithFrame:AAdaptionRectFromFrame(CGRectMake(0, 0, 375, 667)) style:UITableViewStylePlain withTag:100 withDelegate:self withCellName:@"ZHNewTableViewCell" withReuseIdentifier:@"identifer" withRowHeight:30 withRefreshBlock:^(UITableView *sender) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [_tableView.mj_header endRefreshing];
                [self initData];
            });
            
        } withLoadMoreBlock:^(UITableView *sender) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [_tableView.mj_footer endRefreshing];
                [self initData];
            });
        }];
        
    }
    return _tableView;
}






@end
