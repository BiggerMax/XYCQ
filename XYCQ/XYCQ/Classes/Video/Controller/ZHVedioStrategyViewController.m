//
//  ZHVedioStrategyViewController.m
//  XYCQ
//
//  Created by zhangliwen on 2017/8/10.
//  Copyright © 2017年 BiggerMax. All rights reserved.
//

#import "ZHVedioStrategyViewController.h"
#import "ZHVideoCollectionViewCell.h"
@interface ZHVedioStrategyViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong)UICollectionView *collection;

@end

@implementation ZHVedioStrategyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"攻略视频";
    self.view.backgroundColor = [UIColor orangeColor];
    [self.view addSubview:self.collection];
    //刷新
    [self addMJRefresh];
    //网络请求
    [self initData];
}
//刷新
- (void)addMJRefresh{
    
    //刷新
    MJRefreshNormalHeader *headerRefresh = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        //self.loadpage = 1;
        [self initData];
        
    }];
    headerRefresh.automaticallyChangeAlpha = YES;
    headerRefresh.lastUpdatedTimeLabel.hidden = YES;
    self.collection.mj_header = headerRefresh;
    
    //更多
    MJRefreshBackNormalFooter *footerRefresh = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        
        [self initData];
        
    }];
    self.collection.mj_footer.automaticallyChangeAlpha = YES;
    self.collection.mj_footer = footerRefresh;
}
- (void)initData{
    
    [self.collection.mj_header endRefreshing];
    [self.collection.mj_footer endRefreshing];
}
#pragma mark - collection delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 10;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ZHVideoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"reuse" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor greenColor];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"%ld被点击了",indexPath.row);
}

#pragma mark - collection
- (UICollectionView *)collection{
    
    if (!_collection) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = AAdaptionSize(349/2, 349/2 * 9/16 + 40 );
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.sectionInset = UIEdgeInsetsMake(8, 8, 0, 8);
        layout.minimumLineSpacing = AAdaption(8);
        //初始化collectionView
        _collection = [[UICollectionView alloc] initWithFrame:AAdaptionRectFromFrame(CGRectMake(0, 0, 375, 667 - 64)) collectionViewLayout:layout];
        _collection.backgroundColor = [UIColor whiteColor];
        _collection.delegate = self;
        _collection.dataSource = self;
        [_collection registerClass:[ZHVideoCollectionViewCell class] forCellWithReuseIdentifier:@"reuse"];
    }
    return _collection;
}





@end
