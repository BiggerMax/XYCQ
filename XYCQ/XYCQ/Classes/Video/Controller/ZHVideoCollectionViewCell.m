//
//  ZHVideoCollectionViewCell.m
//  XYCQ
//
//  Created by zhangliwen on 2017/8/10.
//  Copyright © 2017年 BiggerMax. All rights reserved.
//

#import "ZHVideoCollectionViewCell.h"

@implementation ZHVideoCollectionViewCell
- (id)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    if (self) {
       //自定义界面
        [self initUI];
    }
    return self;
}
- (void)initUI{

    _label = [[UILabel alloc] initWithFrame:AAdaptionRectFromFrame(CGRectMake(0, 349/2 * 9/16 + 40 - 28, 349/2, 28))];
    _label.backgroundColor = [UIColor blackColor];
    [self addSubview:_label];
}
@end
