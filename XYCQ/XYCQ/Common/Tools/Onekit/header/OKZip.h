//
//  OKZip.h
//  OnekitTool
//
//  Created by 袁杰 on 16/12/27.
//  Copyright © 2016年 onekit.cn. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "SSZipArchive.h"
/*!
 *  文件压缩
 */
@interface OKZip : NSObject

/*!
 *  文件压缩
 *  @param SuccessCallBack  成功返回(压缩包)
 *  @param FailureCallBack 失败返回(错误信息)
 */
+(void)zip:(NSDictionary *)files
        successCallBack:(void(^)(NSFileManager *zip))SuccessCallBack
        failureCallBack:(void(^)(NSError *error))
            FailureCallBack;

/*!
 *  文件解压
 *  @param data 压缩包
 *  @param SuccessCallBack  成功返回(数据字典)
 *  @param FailureCallBack 失败返回(错误信息)
 */
+(void)unzip:(NSData *)data
        succsessCallBack:(void(^)(NSDictionary*dic))            SuccessCallBack
            failureCallBack:(void(^)(NSError *error))
                FailureCallBack;


@end
