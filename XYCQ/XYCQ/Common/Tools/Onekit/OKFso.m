//
//  OKFso.m
//  OnekitTool
//
//  Created by 袁杰 on 17/1/1.
//  Copyright © 2017年 onekit.cn. All rights reserved.
//

#import "OKFso.h"
#import "Onekit.h"
@implementation OKFso

+(BOOL)isExist:(NSString *)path{
   // NSString *paths = [self getPathWithFileName:path];
    NSString *pathString = [OKFso getPathWithFileName:path];
    NSFileManager* fileManager = [NSFileManager defaultManager];
    return [fileManager fileExistsAtPath:pathString];
}

+ (NSString*)getPathWithFileName:(NSString*)fileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *directory = [paths objectAtIndex:0];
    NSLog(@"%@",directory);
    NSString* path = [[NSString alloc] initWithString:
                      [directory stringByAppendingPathComponent:fileName]];
    return path;
}

+(BOOL)deleteData:(NSString *)path{
    NSString *pathString = [OKFso getPathWithFileName:path];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    BOOL succeed = [fileManager removeItemAtPath:pathString error:&error];
    return succeed;
}


//+ (BOOL)saveStringWithString:(NSString*)string fileName:(NSString*)fileName ext:(NSString*)ext
//{
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *directory = [paths objectAtIndex:0];
//    fileName = [[NSString alloc] initWithString:
//                [directory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@",fileName,ext]]];
//    NSStringEncoding encoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
//    NSData *data = [string dataUsingEncoding:encoding];
//    return [data writeToFile:fileName atomically:NO];
//}

+ (BOOL)saveData:(NSObject *)data withPath:(NSString *)path
{
    if (notNull(path))
    {
        return [NSKeyedArchiver archiveRootObject:data toFile:path];
    }
    return NO;
}
+(void)save:(NSString *)path object:(NSObject *)object callback:(void (^)(void))callback{
    NSString *pathString = [OKFso getPathWithFileName:path];
    [self saveData:object withPath:pathString];
    callback();
}
+(void)load:(NSString *)path callback:(void (^)(id data))callback{
    NSString *pathString = [OKFso getPathWithFileName:path];
    callback([NSKeyedUnarchiver unarchiveObjectWithFile:pathString]);
}
+(void)loadData:(NSString *)path callback:(void (^)(NSData *))callback{
    callback([NSData dataWithContentsOfFile:path]);
}
+(void)saveData:(NSString *)path data:(NSData *)data callback:(void (^)(void))callback{
    [data writeToFile:path atomically:YES];
    callback();
}
+(void)loadString:(NSString *)path callback:(void (^)(NSString *))callback{
    NSError* error;
    NSString *string = [NSString stringWithContentsOfFile:path
                                                 encoding:NSUTF8StringEncoding
                                                    error:&error];
    callback(string);
}

+(void)saveString:(NSString *)path string:(NSString *)string callback:(void (^)(void))callback{
    NSError* error;
    [string writeToFile:path atomically:NO encoding:NSUTF8StringEncoding error:&error];
    callback();
}

+(void)loadJSON:(NSString *)path callback:(void (^)(NSDictionary *))callback{
    NSString *pathString = [OKFso getPathWithFileName:path];
    [self loadString:pathString callback:^(NSString *string) {
        callback([OKJson parse:string]);
    }];
}

+(void)saveJSON:(NSString *)path json:(NSDictionary *)json callback:(void (^)(void))callback{
    NSString *pathString = [OKFso getPathWithFileName:path];
    NSString* jsonString = [OKJson stringify:json];
    [self saveString:pathString string:jsonString callback:^{
        callback();
    }];
}


+(void)loadImage:(NSString *)path callback:(void (^)(UIImage *))callback{
    NSString *pathString = [OKFso getPathWithFileName:path];
    callback([UIImage imageWithContentsOfFile:pathString]);
}

+(void)saveImage:(NSString *)path image:(UIImage *)image callback:(void (^)(void))callback{
    NSData *data = UIImagePNGRepresentation(image);
    NSString *pathString = [OKFso getPathWithFileName:path];
    [self saveData:pathString data:data callback:^{
        callback();
    }];
}
+(void)saveBytes:(NSString *)path bytes:(OKBytes *)bytes callback:(void (^)(void))callback{
    NSData *data = [[NSData alloc] initWithBytes:bytes.bytes length:bytes.length];
    NSString *pathString = [OKFso getPathWithFileName:path];
    [self saveData:pathString data:data callback:^{
        callback();
    }];
}

+(void)loadBytes:(NSString *)path callback:(void(^)(OKBytes *))callback{
    NSString *pathString = [OKFso getPathWithFileName:path];
    [self loadData:pathString callback:^(NSData *data) {
        OKBytes* bytes = [OKBytes new];
        bytes.length = data.length;
        bytes.bytes = (Byte*)[data bytes];
        callback(bytes);
    }];
}
@end
