//
//  Const.h
//  GameStrategy
//
//  Created by 袁杰 on 2017/8/9.
//  Copyright © 2017年 BiggerMax. All rights reserved.
//

#ifndef Const_h
#define Const_h

	//DEBUG 模式下打印日志
#ifdef DEBUG
#define NSLog(...) NSLog(__VA_ARGS__)
#else
#define NSLog(...)
#endif

	// 获取RGB颜色
#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define RGB(r,g,b) RGBA(r,g,b,1.0f)

#define ThemeColor RGB(230, 198, 168)
#define RandomColor RGB((arc4random() % 256),(arc4random() % 256),(arc4random() % 256))

#define ScreenWidth [UIScreen mainScreen].bounds.size.width
#define ScreenHeight [UIScreen mainScreen].bounds.size.height

#define Font(size) [UIFont systemFontOfSize:size]

/*判断iphone设备，通过设备的屏幕进行判断*/
#define  iPhone4   (\
CGSizeEqualToSize( [UIScreen mainScreen].bounds.size , CGSizeMake(320, 480)) || CGSizeEqualToSize( [UIScreen mainScreen].bounds.size , CGSizeMake(640, 960)) \
)


#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define iPhone6Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242,2208), [[UIScreen mainScreen] currentMode].size) : NO)

	//如果能执行（currentMode）就说明可以像素来区分，如果等于就返回YES
#define iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)

	//函数编译体
#define IsEmpty(source) !(source&&source.length>0)
#define IsNull(source) [source isKindOfClass:[NSNull class]]
#define  APPDELEGATE ((AppDelegate*)[UIApplication sharedApplication].delegate)


#endif /* Const_h */
