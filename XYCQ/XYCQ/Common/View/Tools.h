//
//  Tools.h
//  童伙妈妈
//
//  Created by Summer on 2016/11/11.
//  Copyright © 2016年 Summer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tools : NSObject

/**  消息展示 */
+ (void)showMessageWithTitle:(NSString * _Nullable)title
                     content:(NSString * _Nullable)content;

/**  消息展示后消失 */
+ (void)showMessageWithTitle:(NSString * _Nullable)title
                     content:(NSString * _Nullable)content
                 disMissTime:(NSTimeInterval)time;
/**  消息展示带按钮事件 */
+ (void)showMessageWithTitle:(NSString * _Nullable)title
                     content:(NSString * _Nullable)content
                buttonTitles:(NSArray <NSString *> * _Nullable)titles
               clickedHandle:(void(^ _Nullable)(NSInteger index))clickedBtn;
/**  消息展示带按钮事件 */
+ (void)showMessageWithTitle:(NSString * _Nullable)title
                     content:(NSString * _Nullable)content
                buttonTitles:(NSArray <NSString *> * _Nullable)titles
               clickedHandle:(void(^ _Nullable)(NSInteger index))clickedBtn
            compeletedHandle:(void(^ _Nullable)())handle;


/**  归档处理操作 */
+ (void)encodeWithObject:(NSObject * _Nullable)encodeObject withcoder:(NSCoder * _Nullable)aCoder;

/**  解档处理操 */
+ (void)unencodeWithObject:(NSObject * _Nullable)unarchObject withcoder:(NSCoder * _Nullable)aDecoder;

/**  将对象转化为字符串 */
+ (NSString * _Nullable)jsonStringWithObject:(id _Nullable)object;

/**  将字典值转化为String类型 */
+ (NSMutableDictionary * _Nullable)valuesForamtToStringWithDict:(NSDictionary * _Nullable)dict;

/**  将数组值转化为String类型 */
+ (NSMutableArray * _Nullable)valuesForamtToStringWithArray:(NSArray * _Nullable)array;

/**  获取当前控制器 */
+ (UIViewController * _Nullable)keyViewController;

/**  获取当前APP版本号 */
+ (NSString * _Nullable)currentSystemVersion;

/**  随机色 */
+ (UIColor * _Nullable)randomColor;

/**  非渲染的图片 */
+ (UIImage * _Nullable)originalImageWithImageName:(NSString * _Nullable)imageName;

//+ (UIImage *)handleImage:(UIImage *)originalImage withSize:(CGSize)size ;
//+ (UIImage *)scaleImage:(UIImage *)image toWidthScale:(float)widthSize toHeightScale:(float)heightSize;



@end
