//
//  RefreshTableView.h
//  RefreshTableView
//
//  Created by Summer on 2016/11/11.
//  Copyright © 2016年 Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^RefreshLoadMoreBlock)(UITableView* sender);

@interface RefreshLoadMoreTableView : UITableView

- (instancetype)initWithFrame:(CGRect)frame
                        style:(UITableViewStyle)style
                      withTag:(NSInteger)tag
                 withDelegate:(id)delegate
                 withCellName:(NSString *)cellName
          withReuseIdentifier:(NSString *)reuseIdentifier
                withRowHeight:(CGFloat)rowHeight
             withRefreshBlock:(RefreshLoadMoreBlock)refresh
            withLoadMoreBlock:(RefreshLoadMoreBlock)loadMore;

@property (nonatomic, copy) RefreshLoadMoreBlock refreshBlock;

@end
