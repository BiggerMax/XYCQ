//
//  UIColor+hex.h
//  绵阳师范教职工端
//
//  Created by zhengbing on 8/9/16.
//  Copyright © 2016 白强. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (hex)

/**
 *    16进制Color
 */
+ (UIColor *)colorWithHex:(long)hexColor;

/**
 *    16进制Color，有透明度
 */
+ (UIColor *)colorWithHex:(long)hexColor alpha:(float)opacity;

@end
