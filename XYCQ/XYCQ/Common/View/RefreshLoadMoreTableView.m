//
//  RefreshTableView.m
//  RefreshTableView
//
//  Created by Summer on 2016/11/11.
//  Copyright © 2016年 Summer. All rights reserved.
//

#import "RefreshLoadMoreTableView.h"
#import "Adaption.h"
#import "Tools.h"

@implementation RefreshLoadMoreTableView

/*
 1:需要两个block参数进行刷新和加载更多的事件回调
 2:cell的name，ReuseIdentifier,delegate,rowHeight
 */

- (instancetype)initWithFrame:(CGRect)frame
                        style:(UITableViewStyle)style
                      withTag:(NSInteger)tag
                 withDelegate:(id)delegate
                 withCellName:(NSString *)cellName
          withReuseIdentifier:(NSString *)reuseIdentifier
                withRowHeight:(CGFloat)rowHeight
             withRefreshBlock:(RefreshLoadMoreBlock)refresh
            withLoadMoreBlock:(RefreshLoadMoreBlock)loadMore{
    //self 必须调用父类进行初始化，不然self为空
    self = [super initWithFrame:frame style:style];
    if (self) {
        //if 语句里面是对除开父类参数以外的参数使用
        self.tag = tag;
        self.delegate = delegate;
        self.dataSource = delegate;
        [self registerClass:NSClassFromString(cellName) forCellReuseIdentifier:reuseIdentifier];
        self.rowHeight = AAdaption(rowHeight);
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
        
//        NSMutableArray *imageArray = [NSMutableArray array];
//        for (NSInteger index = 1; index <= 16; index ++) {
//            UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%ld",index]];
//            image = [Tools scaleImage:image toWidthScale:0.2 toHeightScale:0.2];
//            [imageArray addObject:image];
//        }
        if (refresh) {
            MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
//            [header setImages:imageArray forState:MJRefreshStateIdle];
//            [header setImages:imageArray forState:MJRefreshStatePulling];
//            [header setImages:imageArray forState:MJRefreshStateRefreshing];
            header.lastUpdatedTimeLabel.hidden = NO; // 隐藏时间
            header.stateLabel.hidden = NO;// 隐藏状态
            self.refreshBlock = refresh;
            self.mj_header = header;
            
        }
        if (loadMore) {
            self.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                loadMore(self);
            }];
        }
    }
    return self;
}
-(void)loadNewData{
    self.refreshBlock(self);
}
@end
